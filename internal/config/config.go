package config

import (
	"database/sql"
	"fmt"
	"log"
	"regexp"

	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
)

type Constants struct {
	PORT   string
	Sqlite struct {
		Dblocation string
		Table      string
		Chattable  string
	}
	Joenmt struct {
		Url string
	}
	Mlservice struct {
		Url string
	}

	Sqliteml struct {
		Dblocation string
	}
	Accuracy struct {
		Max float64
	}
}

type Config struct {
	Constants
	Database   *sql.DB
	Regex      *regexp.Regexp
	Mldatabase *sql.DB
	// Tmpls    *template.Template
}

func New() (*Config, error) {
	config := Config{}
	constants, err := initViper()
	config.Constants = constants
	if err != nil {
		return &config, err
	}

	litedb, err := sql.Open("sqlite3", config.Constants.Sqlite.Dblocation)
	if err != nil {
		return &config, err

	}

	config.Database = litedb

	litedbml, err := sql.Open("sqlite3", config.Constants.Sqliteml.Dblocation)
	if err != nil {
		return &config, err

	}

	config.Mldatabase = litedbml

	config.Regex = regexp.MustCompile("[^\\p{L}\\d]+")

	return &config, err
}

func initViper() (Constants, error) {
	viper.SetConfigName("chatproxynlp.config") // Configuration fileName without the .TOML or .YAML extension
	viper.AddConfigPath(".")                   // Search the root directory for the configuration file
	err := viper.ReadInConfig()                // Find and read the config file
	if err != nil {                            // Handle errors reading the config file
		return Constants{}, err
	}
	viper.WatchConfig() // Watch for changes to the configuration file and recompile
	viper.OnConfigChange(func(e fsnotify.Event) {
		fmt.Println("Config file changed:", e.Name)
	})
	viper.SetDefault("PORT", "8000")
	if err = viper.ReadInConfig(); err != nil {
		log.Panicf("Error reading config file, %s", err)
	}

	var constants Constants
	err = viper.Unmarshal(&constants)
	return constants, err
}
