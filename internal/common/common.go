package common

import "time"

func UTCTimeStr2LocalTimeStr(ts, layout string) (string, error) {
	timeObject, err := time.Parse(time.RFC3339, ts)
	if err != nil {
		return "", err
	}

	return time.Unix(timeObject.Unix(), 0).Format(layout), nil
}
