package common

import (
	"log"
	"testing"
	"time"
)

var (

	utstr string
	localstr string
	layoutstr string 
)

func init() {
	utc := time.Now().UTC()
	utstr = utc.String()
	localstr = utc.Local().String()

	layoutstr = "15:04"

	log.Println(utstr,localstr)
}

func TestUTCTimeStr2LocalTimeStr(t *testing.T) {
	type args struct {
		ts     string
		layout string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
		{"test0",args{utstr,layoutstr},layoutstr,true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := UTCTimeStr2LocalTimeStr(tt.args.ts, tt.args.layout)
			if (err != nil) != tt.wantErr {
				t.Errorf("UTCTimeStr2LocalTimeStr() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("UTCTimeStr2LocalTimeStr() = %v, want %v", got, tt.want)
			}
		})
	}
}
