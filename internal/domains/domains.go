package domains

type Updatetemplate struct {
	Intentid int64  `json:"Intentid"`
	Templateold string `json:"Templateold"`
	Templatenew string `json:"Templatenew"`
}


type Newtemplate struct {
	Intentid int64  `json:"Intentid"`
	Template string `json:"Template"`
}

type Newintent struct {
	Intent   string `json:"Intent"`
	Desc     string `json:"Desc"`
	Template string `json:"Template"`
}

type Scorelevel struct {
	Score float64 `json:"Score"`
}

type Mlphrase struct {
	Phrase string `json:"Phrase"`
	Intent int64  `json:"Intent"`
}

type DbChats struct {
	Date     int64  `json:"Date"`
	Chatuuid string `json:"Chatuuid"`
	Count    int64  `json:"Count"`
}
type DbChatsOut struct {
	Date     string `json:"Date"`
	Chatuuid string `json:"Chatuuid"`
	Count    int64  `json:"Count"`
}
type DbTemplate struct {
	Intentid int64  `json:"Intentid"`
	Template string `json:"Template"`
	Intent   string `json:"Intent"`
}

type Intent struct {
	Id     int64  `json:"Id"`
	Intent string `json:"Intent"`
	Desc   string `json:"Desc"`
}

type DbChatuuid struct {
	Chatuuid string `json:"Chatuuid"`
}

type Mlserviceanswer struct {
	Accuracy float64
	Answer   string
}
type Mlserviceask struct {
	Ask string `json:"ask"`
}

type Slot struct {
	Nguid      string `json:"Nguid"`
	Chatuuid   string `json:"Chatuuid"`
	Clname     string `json:"Clname"`
	Clphone    string `json:"Clphone "`
	Clage      int    `json:"Clage"`
	Clcity     string `json:"Clcity"`
	Cllastrimg Image
}

type Image struct {
	Timedid       string
	Id            int
	Name          string
	Age           int
	City          string
	Phone         string
	Img_file_name string
}
type Payload struct {
	Answer string `json:"Answer"`
}

type ChatDialog struct {
	Nguid    string  `json:"Nguid"`
	Chatuuid string  `json:"Chatuuid"`
	Textin   string  `json:"Textin"`
	Textout  string  `json:"Textout"`
	Site     string  `json:"Site"`
	Source   string  `json:"Source"`
	Class    string  `json:"Class"`
	Score    float64 `json:"Score"`
}

type DbChat struct {
	Created int64   `json:"Created"`
	Textin  string  `json:"Textin"`
	Textout string  `json:"Textout"`
	Source  string  `json:"Source"`
	Class   string  `json:"Class"`
	Score   float64 `json:"Score"`
}

type DbChatOut struct {
	Created string  `json:"Created"`
	Textin  string  `json:"Textin"`
	Textout string  `json:"Textout"`
	Source  string  `json:"Source"`
	Class   string  `json:"Class"`
	Score   float64 `json:"Score"`
}

type Ask struct {
	Nguid    string `json:"Nguid"`
	Chatuuid string `json:"Chatuuid"`
	Phone    string `json:"Phone"`
	Text     string `json:"Text"`
	Mob      int    `json:"Mob"`
}

type AskFromJs struct {
	Nguid    string `json:"Nguid"`
	Chatuuid string `json:"Chatuuid"`
	Img      Image  `json:"Img"`

	Message string `json:"Message"`
}

type Aksnlp struct {
	Id  int    `json:"id"`
	Src string `json:"src"`
}

type Aksjoey struct {
	Ask string `json:"ask"`
}

type Answerjoey struct {
	Answer string `json:"Answer"`
}

type Answernlp struct {
	Score  float64 `json:"Score"`
	Answer string  `json:"Answer"`
}

type AnswertoJs struct {
	Answer string `json:"Answer"`
}

type HookAnswer struct {
	Recipient_id string `json:"recipient_id"`
	Text         string `json:"text"`
}

type HookAnswers struct {
	Answers []HookAnswer
}
