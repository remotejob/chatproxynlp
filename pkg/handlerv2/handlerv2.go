package handlerv2

import (
	"bytes"
	"encoding/json"

	"log"
	"net/http"
	"net/url"
	"strings"

	"github.com/go-chi/chi"
	"gitlab.com/remotejob/chatproxynlp/internal/config"
	"gitlab.com/remotejob/chatproxynlp/internal/domains"
	"gitlab.com/remotejob/chatproxynlp/pkg/ads"
	"gitlab.com/remotejob/chatproxynlp/pkg/capfirstchar"
	"gitlab.com/remotejob/chatproxynlp/pkg/mlansgenerator"
	"gitlab.com/remotejob/chatproxynlp/pkg/mlchatdb"
)

type Config struct {
	*config.Config
}

func New(configuration *config.Config) *Config {
	return &Config{configuration}
}

func (config *Config) Routes() *chi.Mux {
	router := chi.NewRouter()

	router.Post("/", config.MlGetAnswerFromMlservice)
	// router.Get("/{uuid}/{phone}/{ask}", config.GetAnswer)
	return router
}

func (config *Config) MlGetAnswerFromMlservice(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var chatmsg domains.AskFromJs
	var chatdialog domains.ChatDialog
	var answer domains.AnswertoJs
	var chathistory int
	var answerfrommlservice domains.Mlserviceanswer
	var source string
	var answerfordb string
	var class string
	var score float64

	referer := r.Header.Get("Referer")

	u, err := url.Parse(referer)
	if err != nil {

		log.Println(err)

	}

	hostname := u.Hostname()

	if hostname == "" {
		chatdialog.Site = "Unk"
	} else {
		chatdialog.Site = hostname
	}

	err = json.NewDecoder(r.Body).Decode(&chatmsg)
	if err != nil {

		log.Println(err)
	}

	chathistory, err = mlchatdb.CheckClient(config.Config, chatmsg)
	if err != nil {

		log.Println(err)
	}
	orgask := chatmsg.Message

	if chathistory == 25 {
		// if chathistory == 3 || chathistory == 5 {
		log.Println("mchat 25", chathistory, chatmsg.Chatuuid)
		ads := ads.Create(chathistory, chatmsg)

		answer = domains.AnswertoJs{ads}
		answerfordb = answer.Answer
		source = "ads"

	} else if chathistory == 35 || chathistory == 40 {

		log.Println("bigchat", chathistory, chatmsg.Chatuuid)

		ads := ads.Create(chathistory, chatmsg)

		answer = domains.AnswertoJs{ads}
		answerfordb = answer.Answer
		source = "ads"

	} else {

		lastaskmsg, err := mlchatdb.GetLastAskmsg(config.Config, chatmsg)
		if err != nil {
			log.Println(err)
		}

		if strings.ToLower(orgask) == strings.ToLower(lastaskmsg) {

			ads := ads.Create(chathistory, chatmsg)
			answer = domains.AnswertoJs{ads}

		} else {

			stask := config.Regex.ReplaceAllString(strings.ToLower(orgask), " ")
			stask = strings.TrimSpace(stask)

			if len(stask) < 3 {

				ads := ads.Create(chathistory, chatmsg)

				answer = domains.AnswertoJs{ads}
				answerfordb = answer.Answer
				source = "ads"

			} else {

				message := domains.Mlserviceask{Ask: strings.ToLower(chatmsg.Message)}

				answerfrommlservice = config.NlpReqMlservice(message)

				log.Println(answerfrommlservice)
				class = answerfrommlservice.Answer
				score = answerfrommlservice.Accuracy

				if answerfrommlservice.Accuracy > config.Accuracy.Max {

					source = "class"

					slot := domains.Slot{
						Nguid:      chatmsg.Nguid,
						Chatuuid:   chatmsg.Chatuuid,
						Clname:     "",
						Clphone:    "",
						Clage:      0,
						Clcity:     "",
						Cllastrimg: chatmsg.Img,
					}

					txtans := mlansgenerator.Generate(config.Config, answerfrommlservice.Answer, slot)

					answer = domains.AnswertoJs{txtans}
					answerfordb = answer.Answer

				} else {

					log.Println("Need Joe", class, score, chatmsg.Message)
					// domains.Aksjoey{chatmsg.Message}
					source = "seq2seq"

					ansJoenmt := config.NlpReqJoenmt(domains.Aksjoey{chatmsg.Message})

					if strings.Index(ansJoenmt.Answer, "en tiedä") != -1 {
						source = "fix"
						ads := ads.Create(chathistory, chatmsg)
						answer = domains.AnswertoJs{ads}
						answerfordb = answer.Answer

					} else if strings.Index(ansJoenmt.Answer, "ei ole") != -1 {

						source = "fix"
						ads := ads.Create(chathistory, chatmsg)
						answer = domains.AnswertoJs{ads}
						answerfordb = answer.Answer

					} else {
						answer = domains.AnswertoJs{ansJoenmt.Answer}
						answerfordb = answer.Answer

					}

				}

				if len(answer.Answer) == 0 {

					ads := ads.Create(chathistory, chatmsg)

					answer = domains.AnswertoJs{ads}
					answerfordb = answer.Answer

				}

			}
		}

	}

	chatdialog.Nguid = chatmsg.Nguid
	chatdialog.Chatuuid = chatmsg.Chatuuid
	chatdialog.Textin = orgask
	chatdialog.Textout = answerfordb
	chatdialog.Site = hostname
	chatdialog.Source = source
	chatdialog.Class = class
	chatdialog.Score = score

	mlchatdb.Insert(config.Config, chatdialog)

	finelanswer := answer.Answer

	answer.Answer = capfirstchar.Cap(finelanswer)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(answer)

}

func (config *Config) NlpReqMlservice(messages domains.Mlserviceask) domains.Mlserviceanswer {

	log.Println(messages)
	bytesRepresentation, err := json.Marshal(messages)
	if err != nil {
		log.Fatalln(err)
	}

	req, err := http.NewRequest("POST", config.Mlservice.Url, bytes.NewBuffer(bytesRepresentation))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json,text/plain, */*")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {

		log.Fatalln(err)
	}
	defer resp.Body.Close()

	var result domains.Mlserviceanswer

	json.NewDecoder(resp.Body).Decode(&result)

	return result

}

func (config *Config) NlpReqJoenmt(messages domains.Aksjoey) domains.Answerjoey {

	log.Println(messages)

	bytesRepresentation, err := json.Marshal(messages)
	if err != nil {
		log.Fatalln(err)
	}

	req, err := http.NewRequest("POST", config.Joenmt.Url, bytes.NewBuffer(bytesRepresentation))
	req.Header.Set("Content-Type", "application/json")
	// req.Header.Set("Accept", "application/json,text/plain, */*")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {

		log.Fatalln("resp", err.Error())
	}
	defer resp.Body.Close()

	var result domains.Answerjoey

	json.NewDecoder(resp.Body).Decode(&result)

	log.Println(result)

	ans := domains.Answerjoey{result.Answer}

	return ans

}
