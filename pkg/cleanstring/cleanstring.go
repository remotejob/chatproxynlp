package cleanstring

import (
	"regexp"
	"strings"
)

func Elab(text string) string {

	reg := regexp.MustCompile("[^\\p{L}\\d_?!.]+")

	preproceed := strings.Fields(text)

	var onlywords []string

	for _, prstr := range preproceed {

		if !strings.HasPrefix(prstr, "@") && !strings.HasPrefix(prstr, "https://") {

			prstr = strings.Replace(prstr, "\n", " ", -1)

			regprstr := reg.ReplaceAllString(prstr, " ")

			if len(regprstr) > 0 {

				onlywords = append(onlywords, regprstr)
			}

		}

	}

	clphrase := strings.Join(onlywords, " ")
	clphrase = strings.Replace(clphrase, "..", ".", -1)
	clphrase = strings.Replace(clphrase, "??", "?", -1)
	clphrase = strings.Replace(clphrase, "!!", "!", -1)
	clphrase = strings.Replace(clphrase, ".", " ", -1)
	clphrase = strings.Replace(clphrase, "!", " ", -1)
	clphrase = strings.Replace(clphrase, "?", " ", -1)
	clphrase = strings.Replace(clphrase, "  ", " ", -1)
	
	clphrase = strings.ToLower(clphrase)

	return strings.TrimSpace(clphrase)

}


