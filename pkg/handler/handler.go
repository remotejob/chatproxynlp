package handler

import (
	"encoding/json"

	"log"
	"net/http"
	"net/url"
	"strings"

	"github.com/go-chi/chi"
	"gitlab.com/remotejob/chatproxynlp/internal/config"
	"gitlab.com/remotejob/chatproxynlp/internal/domains"
	"gitlab.com/remotejob/chatproxynlp/pkg/ads"
	"gitlab.com/remotejob/chatproxynlp/pkg/mlchatdb"
)

type Config struct {
	*config.Config
}

func New(configuration *config.Config) *Config {
	return &Config{configuration}
}

func (config *Config) Routes() *chi.Mux {
	router := chi.NewRouter()

	router.Post("/", config.MlGetAnswerFromRasa)
	// router.Get("/{uuid}/{phone}/{ask}", config.GetAnswer)
	return router
}

func (config *Config) MlGetAnswerFromRasa(w http.ResponseWriter, r *http.Request) {

	var chatmsg domains.AskFromJs
	var chatdialog domains.ChatDialog
	var answer domains.AnswertoJs
	var chathistory int

	referer := r.Header.Get("Referer")

	u, err := url.Parse(referer)
	if err != nil {

		log.Println(err)

	}

	hostname := u.Hostname()

	if hostname == "" {
		chatdialog.Site = "Unk"
	}
	chatdialog.Site = hostname

	err = json.NewDecoder(r.Body).Decode(&chatmsg)
	if err != nil {

		log.Println(err)
	}

	chathistory, err = mlchatdb.CheckClient(config.Config, chatmsg)
	if err != nil {

		log.Println(err)
	}

	orgask := chatmsg.Message


	if chathistory == 10 || chathistory == 25  {

		log.Println("chat",chathistory,chatmsg.Chatuuid)
		ads := ads.Create(chathistory, chatmsg)

		answer = domains.AnswertoJs{ads}

	} else if chathistory == 35 || chathistory == 40 {

		log.Println("big chat",chathistory,chatmsg.Chatuuid)

		ads := ads.Create(chathistory, chatmsg)

		answer = domains.AnswertoJs{ads}

	} else {

		lastaskmsg, err := mlchatdb.GetLastAskmsg(config.Config, chatmsg)
		if err != nil {
			log.Println(err)
		}

		if strings.ToLower(orgask) == strings.ToLower(lastaskmsg) {

			ads := ads.Create(chathistory, chatmsg)
			answer = domains.AnswertoJs{ads}

		} else {

			stask := config.Regex.ReplaceAllString(strings.ToLower(orgask), " ")
			stask = strings.TrimSpace(stask)

			if len(stask) < 3 {

				ads := ads.Create(chathistory, chatmsg)

				answer = domains.AnswertoJs{ads}

			} else {

				// senderId := chatmsg.Nguid + "_" + chatmsg.Chatuuid + "_" + chatmsg.Phone + "_" + chatmsg.Name + "_" + strconv.Itoa(chatmsg.Age) + "_" + chatmsg.City

				// message := domains.AskRasa{senderId, stask}

				// answer = config.NlpReqRasa(message)

				// if len(answer.Answer) == 0 {

				// 	log.Println("answer from RASA EMPTY!! ")
				// 	ads := ads.Create(chathistory, chatmsg)

				// 	answer = domains.AnswertoJs{ads}

				// }

			}
		}

	}

	chatdialog.Nguid = chatmsg.Nguid
	chatdialog.Chatuuid = chatmsg.Chatuuid
	// chatdialog.Mob = chatmsg.Mob
	chatdialog.Textin = orgask
	chatdialog.Textout = answer.Answer

	mlchatdb.Insert(config.Config, chatdialog)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(answer)

}

// func (config *Config) NlpReqRasa(messages domains.AskRasa) domains.AnswertoJs {

// 	// log.Println(messages)

// 	bytesRepresentation, err := json.Marshal(messages)
// 	if err != nil {
// 		log.Fatalln(err)
// 	}

// 	req, err := http.NewRequest("POST", config.Chat.Url, bytes.NewBuffer(bytesRepresentation))
// 	req.Header.Set("Content-Type", "application/json")
// 	req.Header.Set("Accept", "application/json,text/plain, */*")
// 	client := &http.Client{}
// 	resp, err := client.Do(req)
// 	if err != nil {

// 		log.Fatalln(err)
// 	}
// 	defer resp.Body.Close()

// 	var result []domains.RasaAnswer

// 	json.NewDecoder(resp.Body).Decode(&result)

// 	anscap := capfirstchar.Cap(result[0].Text)

// 	ans := domains.AnswertoJs{anscap}

// 	return ans

// }

// func (config *Config) MlGetAnswerFromJoeynmtreverse(w http.ResponseWriter, r *http.Request) {

// 	var chatmsg domains.Ask
// 	var chatdialog domains.ChatDialog

// 	referer := r.Header.Get("Referer")

// 	u, err := url.Parse(referer)
// 	if err != nil {

// 		log.Println(err)

// 	}

// 	hostname := u.Hostname()

// 	if hostname == "" {
// 		chatdialog.Site = "Unk"
// 	}
// 	chatdialog.Site = hostname

// 	err = json.NewDecoder(r.Body).Decode(&chatmsg)
// 	if err != nil {

// 		log.Println(err)
// 	}

// 	log.Println(chatmsg)

// 	orgask := chatmsg.Text
// 	stask := config.Regex.ReplaceAllString(strings.ToLower(orgask), " ")

// 	message := domains.Aksjoey{stask}

// 	answer := config.NlpReqJoenmt(message)

// 	if len(answer.Answer) == 0 {

// 		log.Println("answer from JOE EMPTY!! ")

// 	}

// 	chatdialog.Nguid = chatmsg.Nguid
// 	chatdialog.Chatuuid = chatmsg.Chatuuid
// 	// chatdialog.Mob = chatmsg.Mob
// 	chatdialog.Textin = orgask
// 	chatdialog.Textout = answer.Answer

// 	log.Println(chatdialog)
// 	mlchatdb.Insert(config.Config, chatdialog)

// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(answer)

// }

// func (config *Config) NlpReqJoenmt(messages domains.Aksjoey) domains.Answerjoey {

// 	bytesRepresentation, err := json.Marshal(messages)
// 	if err != nil {
// 		log.Fatalln(err)
// 	}

// 	req, err := http.NewRequest("POST", config.Joenmt.Url, bytes.NewBuffer(bytesRepresentation))
// 	req.Header.Set("Content-Type", "application/json")
// 	req.Header.Set("Accept", "application/json,text/plain, */*")
// 	client := &http.Client{}
// 	resp, err := client.Do(req)
// 	if err != nil {

// 		log.Fatalln(err)
// 	}
// 	defer resp.Body.Close()

// 	var result domains.Answerjoey

// 	json.NewDecoder(resp.Body).Decode(&result)

// 	log.Println(result)

// 	ans := domains.Answerjoey{result.Answer}

// 	return ans

// }
