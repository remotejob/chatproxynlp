package ads

import (
	"math/rand"
	"strings"
	"time"

	"gitlab.com/remotejob/chatproxynlp/internal/domains"
)

func Create(chathistory int, chatmsg domains.AskFromJs) string {

	// var paterns []string
	var patern string

	rand.Seed(time.Now().UnixNano())

	if chathistory == 0 {

		paterns := []string{"Heipä-hei kukas sielä?",
			"Moro-moro, miten menee.",
			"Tere, mitä mies.",
			"Heipä hei kovis!",
			"Hei vaan, kuis menee?",
			"Hei, mitäs kuuluu?",
			"Terve, kuinka surisee",
			"Hei! Hei!",
		}
		rnum := rand.Intn(len(paterns))

		patern = paterns[rnum]

	} else {

		paterns := []string{"Odotan soittoasi..numeroni on: 070095517 name.",
			"Kerro lisää itsestäsi. Soita mulle numeroon 070095517 name.",
			"Soita mulle numeroon 070095517 name.",
			"Odotan soittoasi.. 070095517 name.",
			"Odotan soittoasi.. 070095517 name ..Soitatko?",
			"Odottelen soittoasi numeroon 070095517 name. Soitatko mulle?",
			"Oo kiva ja soita mulle numeroon 070095517 name.",
			"Kerro lisää. Soita mulle numeroon 070095517 name.",
			"Kun soitat minulle? 070095517 name.",
		}

		rand.Seed( time.Now().UnixNano())
		rnum := rand.Intn(len(paterns))

		patern = paterns[rnum]

		patern = strings.Replace(patern, "070095517", chatmsg.Img.Phone, 1)
		patern = strings.Replace(patern, "name", chatmsg.Img.Name, 1)

	}

	return patern

}
