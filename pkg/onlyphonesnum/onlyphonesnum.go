package onlyphonesnum

import "log"

import "strings"

func Elab(nums map[string]string) map[string]string {

	ret := make(map[string]string, 0)

	tocheck := make(map[string]interface{}, 0)

	for k, v := range nums {

		if strings.HasPrefix(k, "05") && len(k) == 10 {

			phhum := strings.Replace(k, "0", "358", 1)

			ret[phhum] = v
		} else if strings.HasPrefix(k, "04") && len(k) == 10 {

			phhum := strings.Replace(k, "0", "358", 1)

			ret[phhum] = v
		} else if strings.HasPrefix(k, "358") && len(k) == 12 {
			ret[k] = v

		} else if strings.HasPrefix(k, "4") && len(k) == 9 {

			ret["358"+k] = v
		} else {

			tocheck[k] = struct{}{}
		}

	}

	for k, _ := range tocheck {

		log.Println(k)

	}

	return ret

}

func ElabOne(nums string) string {

	var ret string

	if strings.HasPrefix(nums, "04") && len(nums) == 10 {

		phhum := strings.Replace(nums, "0", "+358", 1)

		ret = phhum
	} else if strings.HasPrefix(nums, "05") && len(nums) == 10 {

		phhum := strings.Replace(nums, "0", "+358", 1)

		ret = phhum
	} else if strings.HasPrefix(nums, "358") && len(nums) == 12 {
		// ret[k] = v
		ret = nums

	} else if strings.HasPrefix(nums, "4") && len(nums) == 9 {

		// ret["358"+k] = v
		ret = "+358" + nums
	}


	return ret

}
