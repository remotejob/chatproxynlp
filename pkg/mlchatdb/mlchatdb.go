package mlchatdb

import (
	"database/sql"
	"log"
	"strconv"

	"gitlab.com/remotejob/chatproxynlp/internal/config"
	"gitlab.com/remotejob/chatproxynlp/internal/domains"
)

func Updatetemplate(db *sql.DB, entity domains.Updatetemplate) error {

	stmt, err := db.Prepare("update templatestbl set template=? where intentid=? and template=?")
	if err != nil {
		return err
	}
	stmt.Exec(entity.Templatenew, entity.Intentid,entity.Templateold)

	return nil
}


func Inserttemplate(db *sql.DB, entity domains.Newtemplate) error {

	stmt, err := db.Prepare("insert or ignore into templatestbl(intentid,template) values(?,?)")
	if err != nil {
		return err
	}
	stmt.Exec(entity.Intentid, entity.Template)

	return nil
}

func Insertintent(db *sql.DB, entity domains.Newintent) error {

	stmstr := "select max(id) from  intenttbl"

	query, err := db.Prepare(stmstr)

	if err != nil {
		return err

	}

	defer query.Close()
	var lastid int64
	var newid int64

	err = query.QueryRow().Scan(&lastid)

	switch {
	case err == sql.ErrNoRows:

		return err
		// output = "0"
	case err != nil:

		return err

	default:

		newid = lastid + 1

	}

	// log.Println("Newid", newid)

	stmt, err := db.Prepare("insert into intenttbl(id,intent,desc) values(?,?,?)")
	if err != nil {
		return err
	}
	stmt.Exec(newid, entity.Intent, entity.Desc)

	stmt, err = db.Prepare("insert into templatestbl(intentid,template) values(?,?)")
	if err != nil {
		return err
	}
	stmt.Exec(newid, entity.Template)

	return nil
}

func Insertask(db *sql.DB, entity domains.Mlphrase) error {

	stmt, err := db.Prepare("insert or ignore into asktbl(ask,intent) values(?,?)")
	if err != nil {
		return err
	}
	stmt.Exec(entity.Phrase, entity.Intent)

	return nil
}

func GetTeplates(db *sql.DB) ([]domains.DbTemplate, error) {

	var res []domains.DbTemplate
	stmt := "select i.id,t.template,i.intent from templatestbl as t,intenttbl as i where t.intentid=i.id order by i.id"
	q, err := db.Query(stmt)
	if err != nil {

		return nil, err
	}
	for q.Next() {
		var entity domains.DbTemplate

		err = q.Scan(&entity.Intentid, &entity.Template, &entity.Intent)
		if err != nil {
			return nil, err
		}
		res = append(res, entity)

	}

	return res, nil

}

func GetIntents(db *sql.DB) ([]domains.Intent, error) {

	var res []domains.Intent
	stmt := "select id,intent,Desc from intenttbl order by id"

	q, err := db.Query(stmt)
	if err != nil {

		return nil, err
	}
	for q.Next() {
		var intent domains.Intent

		err = q.Scan(&intent.Id, &intent.Intent, &intent.Desc)
		if err != nil {
			return nil, err
		}
		res = append(res, intent)

	}

	return res, nil

}

func GetOneChat(db *sql.DB, id string) ([]domains.DbChat, error) {

	var res []domains.DbChat

	stmt := "select created_at,textin,textout,source,class,score from mlchatdb where chatuuid = ? order by created_at"

	q, err := db.Query(stmt, id)
	if err != nil {

		return nil, err
	}
	for q.Next() {
		var chat domains.DbChat

		err = q.Scan(&chat.Created, &chat.Textin, &chat.Textout, &chat.Source, &chat.Class, &chat.Score)
		if err != nil {
			return nil, err
		}
		res = append(res, chat)

	}

	return res, nil
}

func GetLastChats(db *sql.DB, stmt string) ([]domains.DbChats, error) {

	var res []domains.DbChats

	q, err := db.Query(stmt)
	if err != nil {

		return nil, err
	}
	for q.Next() {
		var entity domains.DbChats

		err = q.Scan(&entity.Date, &entity.Chatuuid, &entity.Count)
		if err != nil {
			return nil, err
		}
		res = append(res, entity)

	}
	return res, nil

}

func Insert(config *config.Config, entity domains.ChatDialog) {

	stmstr := "INSERT INTO " + config.Constants.Sqlite.Chattable + " (nguid,chatuuid,textin,textout,site,source,class,score) VALUES (?,?,?,?,?,?,?,?)"

	statement, err := config.Database.Prepare(stmstr)
	if err != nil {

		log.Panicln(err)

	}

	tx, err := config.Database.Begin()
	if err != nil {

		log.Panicln(err)

	}

	tx.Stmt(statement).Exec(entity.Nguid, entity.Chatuuid, entity.Textin, entity.Textout, entity.Site, entity.Source, entity.Class, entity.Score)

	tx.Commit()

}

func CheckClient(config *config.Config, entity domains.AskFromJs) (int, error) {

	var output string
	var outerr error
	var outputint int

	outerr = nil

	stmstr := "select count(*) from " + config.Constants.Sqlite.Chattable + " where  chatuuid=?"

	query, err := config.Database.Prepare(stmstr)

	if err != nil {
		outerr = err

	}

	defer query.Close()

	err = query.QueryRow(entity.Chatuuid).Scan(&output)

	switch {
	case err == sql.ErrNoRows:

		log.Println("No chat wit that ID so NEW!!")
		// output = "0"
	case err != nil:
		outerr = err

	default:

		i1, err := strconv.Atoi(output)
		if err == nil {
			outputint = i1
		} else {
			outerr = err
		}

	}

	return outputint, outerr
}

func GetLastAskmsg(config *config.Config, entity domains.AskFromJs) (string, error) {

	var output string
	var outerr error

	outerr = nil

	stmstr := "select textin from " + config.Constants.Sqlite.Chattable + " where chatuuid=? order by created_at desc limit 1"

	query, err := config.Database.Prepare(stmstr)

	if err != nil {
		outerr = err

	}

	defer query.Close()

	err = query.QueryRow(entity.Chatuuid).Scan(&output)

	switch {
	case err == sql.ErrNoRows:

		log.Println("No chat wit that ID so NEW!!")
		// output = "0"
	case err != nil:
		outerr = err

	}

	return output, outerr
}
