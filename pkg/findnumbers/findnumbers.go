package findnumbers

import (
	"log"
	"regexp"
)

func Findall(lines [][]string) map[string]string {

	res := make(map[string]string, 0)

	re := regexp.MustCompile("[0-9]+")

	for _, ln := range lines {

		num := re.FindAllString(ln[1], -1)

		if len(num) > 0 {

			if len(num[0]) > 5 {
				log.Println(ln)
				// res = append(res, num[0])
				res[num[0]] = ln[0]
			}
		}

	}

	return res
}
func Findone(line string) map[string]interface{} {

	res := make(map[string]interface{}, 0)

	re := regexp.MustCompile("[0-9]+")

	num := re.FindAllString(line, -1)

	if len(num) > 0 {

		if len(num[0]) > 5 {
			// log.Println(ln)
			// res = append(res, num[0])
			res[num[0]] = struct{}{}
		}
	}

	return res
}
