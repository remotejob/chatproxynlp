package mldatahandlerv0

import (
	"encoding/json"
	"math"
	"net/http"
	"time"

	"github.com/go-chi/chi"
	"gitlab.com/remotejob/chatproxynlp/internal/config"
	"gitlab.com/remotejob/chatproxynlp/internal/domains"
	"gitlab.com/remotejob/chatproxynlp/pkg/cleanstring"
	"gitlab.com/remotejob/chatproxynlp/pkg/mlchatdb"
)

type Config struct {
	*config.Config
}

func New(configuration *config.Config) *Config {
	return &Config{configuration}
}

func (config *Config) Routes() *chi.Mux {
	router := chi.NewRouter()

	router.Get("/chats", config.GetLatestChats)
	router.Post("/chat", config.GetOneChat)
	router.Get("/intents", config.GetIntents)
	router.Get("/templates", config.GetTemplates)
	router.Post("/insertask", config.InsertMlask)
	router.Get("/score", config.Score)
	router.Post("/newintent", config.Newintent)
	router.Post("/newtemplate", config.Newtemplate)
	router.Post("/updatetemplate", config.Updatetemplate)

	return router
}

func (config *Config) Updatetemplate(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var entity domains.Updatetemplate

	err := json.NewDecoder(r.Body).Decode(&entity)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = mlchatdb.Updatetemplate(config.Mldatabase, entity)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (config *Config) Newtemplate(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var entity domains.Newtemplate

	err := json.NewDecoder(r.Body).Decode(&entity)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = mlchatdb.Inserttemplate(config.Mldatabase, entity)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (config *Config) Newintent(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var entity domains.Newintent

	err := json.NewDecoder(r.Body).Decode(&entity)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = mlchatdb.Insertintent(config.Mldatabase, entity)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

}

func (config *Config) Score(w http.ResponseWriter, r *http.Request) {

	res := domains.Scorelevel{config.Constants.Accuracy.Max}

	w.Header().Set("Content-Type", "application/json")

	json.NewEncoder(w).Encode(res)

}

func (config *Config) InsertMlask(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var dbmlask domains.Mlphrase

	err := json.NewDecoder(r.Body).Decode(&dbmlask)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = mlchatdb.Insertask(config.Mldatabase, dbmlask)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

}

func (config *Config) GetTemplates(w http.ResponseWriter, r *http.Request) {

	res, err := mlchatdb.GetTeplates(config.Mldatabase)
	if err != nil {

		http.Error(w, err.Error(), http.StatusForbidden)

	}
	w.Header().Set("Content-Type", "application/json")

	json.NewEncoder(w).Encode(res)

}

func (config *Config) GetIntents(w http.ResponseWriter, r *http.Request) {

	res, err := mlchatdb.GetIntents(config.Mldatabase)
	if err != nil {

		http.Error(w, err.Error(), http.StatusForbidden)

	}
	w.Header().Set("Content-Type", "application/json")

	json.NewEncoder(w).Encode(res)

}

func (config *Config) GetLatestChats(w http.ResponseWriter, r *http.Request) {

	smt := "select min(created_at) as date,chatuuid,count(chatuuid) as count from mlchatdb where datetime(created_at,'unixepoch') >= Datetime('now', '-3 day') group by chatuuid order by date desc"

	mapres, err := mlchatdb.GetLastChats(config.Database, smt)
	if err != nil {

		http.Error(w, err.Error(), http.StatusForbidden)

	}

	location, _ := time.LoadLocation("Europe/Helsinki")

	var res []domains.DbChatsOut
	for _, ent := range mapres {
		utc := time.Unix(ent.Date, 0).UTC()
		var rec domains.DbChatsOut
		rec.Date = utc.In(location).Format("2006-01-02 15:04:05")
		rec.Chatuuid = ent.Chatuuid
		rec.Count = ent.Count

		res = append(res, rec)

	}

	w.Header().Set("Content-Type", "application/json")

	json.NewEncoder(w).Encode(res)

}

func (config *Config) GetOneChat(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var dbchatuuid domains.DbChatuuid

	err := json.NewDecoder(r.Body).Decode(&dbchatuuid)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	mapres, err := mlchatdb.GetOneChat(config.Database, dbchatuuid.Chatuuid)
	if err != nil {

		http.Error(w, err.Error(), http.StatusForbidden)

	}
	var resout []domains.DbChatOut

	location, err := time.LoadLocation("Europe/Helsinki")
	for _, rec := range mapres {

		var recOut domains.DbChatOut
		utc := time.Unix(rec.Created, 0).UTC()
		recOut.Created = utc.In(location).Format("2006-01-02 15:04:05")

		recOut.Textin = cleanstring.Elab(rec.Textin)
		recOut.Textout = rec.Textout
		recOut.Source = rec.Source
		recOut.Class = rec.Class
		recOut.Score = math.Round(rec.Score*100) / 100

		resout = append(resout, recOut)

	}

	w.Header().Set("Content-Type", "application/json")

	json.NewEncoder(w).Encode(resout)

}
