package mlansgenerator

import (
	"bytes"
	"log"
	"text/template"

	"gitlab.com/remotejob/chatproxynlp/internal/config"
	"gitlab.com/remotejob/chatproxynlp/internal/domains"
	"gitlab.com/remotejob/chatproxynlp/pkg/dbhandler"
)

func Generate(conf *config.Config, prediction string, msg domains.Slot) string {

	tmplstr,err := dbhandler.GetOneTemplates(conf.Mldatabase,prediction)
	if err !=nil {

		log.Fatalln(err)
	}
	log.Println(msg)

	var outres string
	var tmplBytes bytes.Buffer

	t := template.Must(template.New(prediction).Parse(tmplstr))

	err = t.Execute(&tmplBytes, msg.Cllastrimg)
	if err != nil {

		log.Panicln(err)

	}
	outres = tmplBytes.String()

	// log.Println("outres",outres)
	return outres

}
