package dbhandler

import "database/sql"

func GetOneTemplates(db *sql.DB, intent string) (string, error) {

	sqlStatement := "SELECT template FROM intenttbl as intd,templatestbl as tmpl WHERE intd.id=tmpl.intentid and intd.intent=? ORDER BY RANDOM() LIMIT 1"

	var ret string

	row := db.QueryRow(sqlStatement, intent)
	switch err := row.Scan(&ret); err {
	case sql.ErrNoRows:
		return ret, err
	case nil:

		return ret, nil
	default:

		return ret, err

	}

}

func GetAll(db *sql.DB) ([][]string, error) {

	var ret [][]string

	q, err := db.Query("SELECT nguid,textin FROM mlchatdb")
	if err != nil {

		return nil, err
	}

	for q.Next() {
		var nguid string
		var textin string
		err = q.Scan(&nguid, &textin)
		if err != nil {
			return nil, err
		}

		ret = append(ret, []string{nguid, textin})

	}

	return ret, nil

}
