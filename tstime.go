package main

import (
	"log"
	"time"
)

func main() {

	utc := time.Now().UTC()
	local := utc
	location, err := time.LoadLocation("Europe/Helsinki")
	if err == nil {
		local = local.In(location)
	}
	log.Println("UTC", utc.Format("15:04"), local.Location(), local.Format("2006-01-02 15:04:05"))

}
