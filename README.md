# chatproxynlp
https://towardsdatascience.com/too-big-to-deploy-how-gpt-2-is-breaking-production-63ab29f0897c

go list -u -m all
go mod tidy
go get -u all

go run gitlab.com/remotejob/chatproxynlp/cmd/server

curl --referer  http://www.fi/ -X POST -H "Content-Type:application/json" -d '{"nguid":"ng0000", "chatuuid":"0005","phone":"0600414115","text":"sdulla on ihanat rinnat"}' localhost:7000/v1/api/chat


CREATE TABLE mlchatdb (created_at INT NOT NULL DEFAULT (strftime('%s', 'now')), nguid STRING NOT NULL, chatuuid STRING NOT NULL, textin STRING NOT NULL,textout STRING NOT NULL, site STRING NOT NULL);

alter table mlchatdb add column mob int default 0;

23.09.2019
reflex -s go run gitlab.com/remotejob/chatproxynlp/cmd/rasawebhook


scp root@165.227.71.179:/root/chatproxynlp/statnlp.db .

13.01.2020
go run gitlab.com/remotejob/chatproxynlp/cmd/findphonenum
go run gitlab.com/remotejob/chatproxynlp/cmd/realtimedbparser




CREATE TABLE chatclients (created_at DATETIME DEFAULT CURRENT_TIMESTAMP, update_at DATETIME DEFAULT CURRENT_TIMESTAMP,chatid varchar(100),clname varchar(20),clphone varchar(20),clage int,clcity varchar,name varchar(20), phone varchar(20),age int, city varchar, imgid int,img_file_name varchar(100));

CREATE UNIQUE INDEX inx_chatid on chatclients(chatid);

31.01.2020
reflex -s go run gitlab.com/remotejob/chatproxynlp/cmd/server


sqlite3 mldata.db
CREATE TABLE intenttbl(id INTEGER PRIMARY KEY,intent varchar(50) NOT NULL);
CREATE TABLE templatestbl(intendid INTEGER, template text NOT NULL);
CREATE TABLE templatestbltmp(intentid INTEGER, template text NOT NULL);

INSERT INTO templatestbltmp(intentid,template) select intendid,template from templatestbl;
DROP TABLE templatestbl;
ALTER TABLE templatestbltmp RENAME TO templatestbl;

UPDATE templatestbl set template=REPLACE(template,'{{phone}}','{{.phone}}');
UPDATE templatestbl set template=REPLACE(template,'{{name}}','{{.name}}');
UPDATE templatestbl set template=REPLACE(template,'{{age}}','{{.age}}');
UPDATE templatestbl set template=REPLACE(template,'{{city}}','{{.city}}');

UPDATE templatestbl set template=REPLACE(template,'{{.phone}}','{{.Phone}}');
UPDATE templatestbl set template=REPLACE(template,'{{.name}}','{{.Name}}');
UPDATE templatestbl set template=REPLACE(template,'{{.age}}','{{.Age}}');
UPDATE templatestbl set template=REPLACE(template,'{{.city}}','{{.City}}');


04.02.2020
on statnlp.db
alter table mlchatdb add column source varchar(50);
alter table mlchatdb add column class varchar(50);
alter table mlchatdb add column score REAL;

on mldata.db
create UNIQUE INDEX idx_ask on askdb(ask);

CREATE TABLE asktbl (
	ask TEXT NOT NULL,
	intent INTEGER NOT NULL,
    UNIQUE(ask,intent)
);

scp root@165.227.101.53:/root/chatproxynlp/statnlp.db .



sqlite3  statnlp.db 'select lower(textin) from  mlchatdb where textout like "Ei ole%" order by textin;' >eiole.txt


awk 'NF>1' file

sed 's/\ \ / /'g 2eiole.txt 

awk 'NF>2' 2eiole.txt >3eiole.txt
awk 'NF<16' 3eiole.txt > tmp

sed -i 's/\ \ / /'g 3eiole.txt
awk 'NF<16' 3eiole.txt > tmp
mv tmp 3eiole.txt 
sort 3eiole.txt | uniq > tmp

11.02.2020

curl -H "Content-Type: application/json" --request POST --data '{"Chatuuid":"014ed33a-a107-4e0e-aacd-33500063f989"}' localhost:7000/v0/mldata/chat

curl -H "Content-Type: application/json" --request POST --data '{"Chatuuid":"014ed33a-a107-4e0e-aacd-33500063f989"}' https://gochatfunctionzeit-git-2-cors.remotejob.now.sh/api/chat

curl -H "Content-Type: application/json" --request POST --data '{"Chatuuid":"014ed33a-a107-4e0e-aacd-33500063f989"}' http://165.227.101.53:7000/v0/mldata/chat
curl -H "Content-Type: application/json" http://165.227.101.53:7000/v0/mldata/intents


12.02.2020

CREATE TABLE intenttbltmp(id INTEGER PRIMARY KEY,intent varchar(50) NOT NULL, "desc" TEXT NOT NULL default 'EDIT');
INSERT INTO intenttbltmp(id,intent) select id,intent from intenttbl;
DROP TABLE intenttbl;
ALTER TABLE intenttbltmp RENAME TO intenttbl;

curl -H "Content-Type: application/json" https://gochatfunctionzeit-git-2-cors.remotejob.now.sh/api/intents

curl -H "Content-Type: application/json" http://165.227.101.53:7000/v0/mldata/templates
curl -H "Content-Type: application/json" https://gochatfunctionzeit-git-2-cors.remotejob.now.sh/api/templates


curl -H "Content-Type: application/json" http://165.227.101.53:7000/v0/mldata/chats
curl -H "Content-Type: application/json" https://gochatfunctionzeit-git-2-cors.remotejob.now.sh/api/chats

curl -H "Content-Type: application/json" --request POST --data '{"Phrase":"test","Intent":1}' http://165.227.101.53:7000/v0/mldata/insertmlask
curl -H "Content-Type: application/json" --request POST --data '{"Phrase":"test","Intent":1}' https://gochatfunctionzeit-git-2-cors.remotejob.now.sh/api/insertmlask

curl -H "Content-Type: application/json" http://165.227.101.53:7000/v0/mldata/score
curl -H "Content-Type: application/json" https://gochatfunctionzeit-git-2-cors.remotejob.now.sh/api/score

curl -H "Content-Type: application/json" -d '{"Intent":"testi","Desc":"desc","Template":"templtest"}' http://165.227.101.53:7000/v0/mldata/newintent
curl -H "Content-Type: application/json" -d '{"Intent":"testi","Desc":"desc","Template":"templtest"}' https://gochatfunctionzeit-git-2-cors.remotejob.now.sh/api/newintent




curl -H "Content-Type: application/json" -d '{"Intentid":1,"Template":"templtest"}' http://165.227.101.53:7000/v0/mldata/newtemplate



17.02.2020
delete from templatestbl where oid=87;
create UNIQUE index idx_id_template on templatestbl(intentid,template);
create UNIQUE index idx_id_template on templatestbl(template,intentid);
drop index idx_id_template;

select template,count(*) from templatestbl group by template;

19.02.2020
curl -H "Content-Type: application/json" -d '{"Intentid":1,"Templateold":"Hei!","Templatenew":"Hei!!"}' http://165.227.101.53:7000/v0/mldata/updatetemplate

21.02.2020
alter table asktbl add column date datetime not null default current_timestamp;
CREATE TABLE asktbltmp(intentid INTEGER, template text NOT NULL);

CREATE TABLE "asktbltmp" (
ask TEXT NOT NULL,
intent INTEGER NOT NULL,date datetime default current_timestamp,
    UNIQUE(ask,intent)
);

INSERT INTO asktbltmp(ask,intent) select ask,intent from asktbl;
DROP TABLE asktbl;
ALTER TABLE asktbltmp RENAME TO asktbl;


