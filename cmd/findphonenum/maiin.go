package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/remotejob/chatproxynlp/pkg/dbhandler"
	"gitlab.com/remotejob/chatproxynlp/pkg/findnumbers"
	"gitlab.com/remotejob/chatproxynlp/pkg/onlyphonesnum"
)

var (
	db  *sql.DB
	err error
)

func init() {

	db, err = sql.Open("sqlite3", "statnlp.db")
	if err != nil {
		log.Fatalf("Error: %v", err)
		return
	}

}

func main() {

	defer db.Close()
	ln, err := dbhandler.GetAll(db)
	if err != nil {

		log.Fatalln(err)
	}

	nums := findnumbers.Findall(ln)

	phonenums := onlyphonesnum.Elab(nums)

	fi, err := os.Create("insertphonenum.sql")
	if err != nil {
		panic(err)
	}
	// close fi on exit and check for its returned error
	defer func() {
		if err := fi.Close(); err != nil {
			panic(err)
		}
	}()

	for k, _ := range phonenums {

		ins := "insert into smsout(phone) values('+" + k + "');"
		fmt.Fprintln(fi, ins)

	}

}
