package main

import (
	"database/sql"
	"encoding/json"
	"io/ioutil"
	"log"
	"os"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/remotejob/chatproxynlp/pkg/findnumbers"
	"gitlab.com/remotejob/chatproxynlp/pkg/onlyphonesnum"
)

var (
	db  *sql.DB
	err error
)

func init() {
	db, err = sql.Open("sqlite3", "chatclients.db")
	if err != nil {
		log.Fatalf("Error: %v", err)
		return
	}
}

func main() {
	jsonFile, err := os.Open("fir-vuechatv6-export.json")
	if err != nil {
		return
	}
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	var result map[string]interface{}
	json.Unmarshal([]byte(byteValue), &result)

	rooms := result["room"]

	myrooms := rooms.(map[string]interface{})

	var chatclid string
	var name string
	var phone string
	var age float64
	var imgid float64
	var img_file_name string
	var serverTimeStamp float64

	stmt, err := db.Prepare("REPLACE INTO chatclients(chatid,clphone, name, phone,age,imgid,img_file_name) values(?,?,?,?,?,?,?)")
	if err != nil {
		log.Panicln(err)
	}

	for k, v := range myrooms {

		chatclid = k

		mychats := v.(map[string]interface{})

		for _, ch := range mychats {

			onechat := ch.(map[string]interface{})

			for _, msg := range onechat {
				// chatclid = k
				// log.Println(msg)
				onemsg := msg.(map[string]interface{})

				for _, v := range onemsg {

					if v == "join" {

						name = onemsg["Name"].(string)
						phone = onemsg["Phone"].(string)
						age = onemsg["Age"].(float64)
						imgid = onemsg["Id"].(float64)
						img_file_name = onemsg["Img_file_name"].(string)
						serverTimeStamp = onemsg["serverTimeStamp"].(float64)

					} else if v == "newmsg" {

						if onemsg["user"] == "mina" {

							nums := findnumbers.Findone(onemsg["message"].(string))

							if len(nums) > 0 {

								for k, _ := range nums {

									phonenum := onlyphonesnum.ElabOne(k)

									if len(phonenum) > 0 {

										log.Println(phonenum, chatclid, name, phone, age, imgid, img_file_name, int64(serverTimeStamp))

										_,err := stmt.Exec(chatclid,phonenum,name, phone, age, imgid, img_file_name)
										if err !=nil {

											log.Panicln(err)
										}

									}
								}

							}
						}
					}
				}
			}
		}

	}

}
