
// not used for now

package main

import (
	"encoding/json"
	"io/ioutil"

	// "io/ioutil"
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
	"gitlab.com/remotejob/chatproxynlp/internal/domains"
)

func routes() *chi.Mux {
	router := chi.NewRouter()
	router.Use(
		render.SetContentType(render.ContentTypeJSON),
		middleware.Logger,
		middleware.DefaultCompress,
		middleware.RedirectSlashes,
		middleware.Recoverer,
		middleware.RealIP,
	)
	router.Post("/webhook", getAnswer)
	return router
}

func getAnswer(w http.ResponseWriter, r *http.Request) {

	// name = strings.ToLower(name)
	for k, v := range r.Header {
		//   request = append(request, fmt.Sprintf("%v: %v", name, h))
		log.Println(k, v)
	}

	w.WriteHeader(200)

	render.Status(r, 200)

	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		panic(err)
	}
	log.Println(string(body))

	var answers []domains.HookAnswer
	answer := domains.HookAnswer{"id", "Hello from hook"}

	answers = append(answers, answer)


	in := []byte(`{"events":[],"responses":[{"text":"Hello World!"}]}`)
    var raw map[string]interface{}
    json.Unmarshal(in, &raw)
    // raw["count"] = 1
    out, _ := json.Marshal(raw)
    log.Println(string(out))
	// jsonanswer, _ := json.Marshal(answers)

	// log.Println(string(jsonanswer))

	// log.Println("start webhook")
	w.Header().Set("Content-Type", "application/json")
	w.Write(out)
	// json.NewEncoder(w).Encode(answers)

	return
}

func main() {

	router := routes()

	walkFunc := func(method string, route string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) error {
		log.Printf("%s %s\n", method, route)
		return nil
	}
	if err := chi.Walk(router, walkFunc); err != nil {
		log.Panicf("Loggin err: %s\n", err.Error())
	}
	// log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", "5055"), router))
	http.ListenAndServe(":5055", router)
}


// {"events":[],"responses":[{"text":"Hello World!"}]}
// r := chi.NewRouter()
// r.Use(
// 	render.SetContentType(render.ContentTypeJSON),
// )

// }

// r.Post("/webhook", func(w http.ResponseWriter, r *http.Request) {
// 	body, err := ioutil.ReadAll(r.Body)

// 	if err != nil {
// 		panic(err)
// 	}
// 	log.Println(string(body))

// 	var answers []domains.HookAnswer
// 	answer := domains.HookAnswer{"id", "Hello from hook"}

// 	answers = append(answers,answer)
// 	// jsonanswer, _ := json.Marshal(answers)

// 	// log.Println(string(jsonanswer))

// 	// log.Println("start webhook")
// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(answers)
// 	// w.Write(jsonanswer)
// })
// http.ListenAndServe(":5055", r)
// }
